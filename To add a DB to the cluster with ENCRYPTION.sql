--USE webTMAdev
--GO
 
---- Create a Database Encryption Key
--CREATE DATABASE ENCRYPTION KEY
--   WITH Algorithm = AES_128
--   ENCRYPTION BY Server Certificate TDEServerCertificate;
 
---- Enable the Database for Encryption by TDE
--ALTER DATABASE webTMAdev
--   SET ENCRYPTION ON; 

-- Check to see if Database encryption has been completed....

--select * from sys.dm_database_encryption_keys

--Step 6
--USE [master] 
--go 
--ALTER AVAILABILITY GROUP HDSvDEV14AGC01 ADD DATABASE webTMAdev

-- Step 9 - do on Secondary server and third server .....

USE [master] 
go 
ALTER DATABASE webTMAdev SET HADR AVAILABILITY GROUP = HDSvDEV14AGC01; 

