-- Restore/setup TDE certificates and keys from backup files ready to switch on TDE for a DB 
-- or restore a backup already encrypted by TDE.

declare @PassPhrase varchar(255)
declare @BackupFolder varchar(512)
declare @ExecSQL varchar(512)
declare @DateStr varchar(32)
declare @Debug int

-- ** WHILE NOBODY ELSE CAN SEE **
-- This is to be manually set by Gina
-- ** Do not save the file once set **
set @PassPhrase = 'XX_KEY_XX'
set @BackupFolder = 'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\TDE\'
set @Debug = 1

print 'Restoring master key from file'
use master
set @ExecSQL = 'restore master key from file = ''' + @BackupFolder + 'TDEServerMasterKey.key''' +
' decryption by password = ''' + @PassPhrase + '''' + 
' encryption by password = ''' + @PassPhrase + ''''
if @Debug = 1 print @ExecSQL
exec(@ExecSQL)

/*
restore master key from file = 'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\TDE\TDEServerMasterKey.key' 
decryption by password = 'XX_KEY_XX' 
encryption by password = 'XX_KEY_XX'
*/

print 'Encrypting master key with service master key'
use master
set @ExecSQL = 'open master key decryption by password = ''' + @PassPhrase + ''''
if @Debug = 1 print @ExecSQL
exec(@ExecSQL)

alter master key add encryption by service master key

/*
open master key decryption by password = 'XX_KEY_XX'
*/

print 'Restoring certificate from file'
use master
set @ExecSQL = 
   'create certificate TDEServerCertificate from file = ''' + @BackupFolder + 'TDEServerCertificate.cer''' +
   ' with private key (file = ''' + @BackupFolder + 'TDEServerCertificate.key''' +
   ', decryption by password = ''' + @PassPhrase + ''')'
if @Debug = 1 print @ExecSQL
exec(@ExecSQL)

/*
create certificate TDEServerCertificate
from file = 'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\TDE\TDEServerCertificate.cer'
with private key
(
   file = 'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\TDE\TDEServerCertificate.key',
   decryption by password = 'XX_KEY_XX'
)
*/

/*print 'Creating AdventureWorks2012 Database Encryption Key'
use AdventureWorks2012
create database encryption key with algorithm = aes_128 encryption by server certificate TDEServerCertificate
*/

print ''
print 'SQL Server is now ready to restore an encrypted AdventureWorks2012 backup or encrypt an existing unencrypted database via TurnOnTDE'
